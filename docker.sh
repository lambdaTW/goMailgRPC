docker build --build-arg BUILD_PATH=server -t mailserver . 
docker build --build-arg BUILD_PATH=client -t mailclient . 

docker network
docker network ls
docker network create grpc
docker network inspect grpc
docker run --network=grpc --env-file=env --name server mailserver
docker run -p 8080:8080 --network=grpc --name client mailclient