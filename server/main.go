package main

import (
	"log"
	"net"
	"os"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"myMail/pb"

	gomail "gopkg.in/gomail.v2"
)

type server struct{}

var ch = make(chan *gomail.Message)

func (s *server) Send(ctx context.Context, mail *pb.MailRequest) (*pb.MailStatus, error) {
	m := gomail.NewMessage()
	m.SetHeader("From", mail.From)
	m.SetHeader("To", mail.To...)
	m.SetHeader("Subject", mail.Subject)
	m.SetBody(mail.Type, mail.Body)
	ch <- m
	return &pb.MailStatus{Status: int32(0), Code: ""}, nil
}

func main() {
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("無法監聽該埠口：%v", err)
	}
	s := grpc.NewServer()
	pb.RegisterMailServer(s, &server{})
	reflection.Register(s)
	go func() {
		d := gomail.NewDialer("smtp.gmail.com", 587, "ntcb10246032@gmail.com", os.Getenv("GMAIL_PASS"))

		var s gomail.SendCloser
		var err error
		open := false
		for {
			select {
			case m, ok := <-ch:
				if !ok {
					return
				}
				if !open {
					if s, err = d.Dial(); err != nil {
						panic(err)
					}
					open = true
				}
				if err := gomail.Send(s, m); err != nil {
					log.Print(err)
				}
			// Close the connection to the SMTP server if no email was sent in
			// the last 30 seconds.
			case <-time.After(30 * time.Second):
				if open {
					if err := s.Close(); err != nil {
						panic(err)
					}
					open = false
				}
			}
		}
	}()
	if err := s.Serve(lis); err != nil {
		log.Fatalf("無法提供服務：%v", err)
		close(ch)
	}
}

func send(mail *pb.MailRequest) error {
	m := gomail.NewMessage()
	m.SetHeader("From", mail.From)
	m.SetHeader("To", mail.To...)
	// m.SetAddressHeader("Cc", mail.Cc[0], mail.Cc[1])
	m.SetHeader("Subject", mail.Subject)
	m.SetBody(mail.Type, mail.Body)
	// m.Attach(".jpg")

	d := gomail.NewDialer("smtp.gmail.com", 587, mail.From, os.Getenv("GMAIL_PASS"))

	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}
