FROM golang AS build-env
ARG BUILD_PATH
ADD . /go/src/myMail
RUN cd /go/src/myMail/$BUILD_PATH && go get && GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o app

FROM alpine
ARG BUILD_PATH
WORKDIR /app
COPY --from=build-env /go/src/myMail/$BUILD_PATH/app /app/
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
EXPOSE 50051
EXPOSE 8080
CMD [ "/app/app" ]