package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"myMail/pb"

	"google.golang.org/grpc"
)

func main() {
	// 連線到遠端 gRPC 伺服器。
	conn, err := grpc.Dial("server:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("連線失敗：%v", err)
	}
	defer conn.Close()

	// 建立新的 Mail 客戶端，所以等一下就能夠使用 Mail 的所有方法。
	c := pb.NewMailClient(conn)

	// 傳送新請求到遠端 gRPC 伺服器 Mail 中，並呼叫 Send 函式
	mr := pb.MailRequest{
		From:    "ntcb10246032@gmail.com",
		To:      []string{"10246032@ntub.edu.tw"},
		Cc:      []string{},
		Subject: "How to use gRPC",
		Body:    "Just done",
		Type:    "text/html",
	}
	http.HandleFunc("/bar", func(w http.ResponseWriter, r *http.Request) {
		ret, err := c.Send(context.Background(), &mr)
		if err != nil {
			log.Fatalf("無法執行 Send 函式：%v", err)
		} else {
			fmt.Fprintf(w, "Send %s", ret.Code)
		}
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}
